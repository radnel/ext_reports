'use strict';
const MainPopup = function () {
	this.constructor();
};

MainPopup.prototype = {
	constructor() {
		return this.initialize();
	},
	initialize() {

		this.initPoopupOptions()
			.initUI()
			.generateDateSelect()
			.showCredentials()
			.tabsActions()
			.showJiraslinks()
			.getReport(true)
			.initListeners()
			.checkAccountID();

		window.bg_wnd = chrome.extension.getBackgroundPage();

		return this;
	},

	initPoopupOptions() {
		this.sayTo       = chrome.extension.getBackgroundPage();
		this.toDay       = new Date();
		this.send_report = {};
		this.report_hash = (localStorage.getItem("reportHash")) ? localStorage.getItem("reportHash") : '';
		this.credentials = localStorage.getItem("credentials") && localStorage.getItem("credentials").length ? JSON.parse(localStorage.getItem("credentials")) : {};
		this.accountId = this._getAccountInfo().accountId || '';

		return this;
	},

	initUI() {

		this.$loader          = $('#loader');
		this.$reportHash      = $('#reportHash');
		this.$reportDate      = $('#reportDate');
		this.$trackedTime     = $('#trackedTime');
		this.$preview         = $('#preview');
		this.$handover        = $('#handover');
		this.$generatorTab    = $('.generator-tab');
		this.$settings        = $('.settings');
		this.$tab_item        = $('.tab-item');
		this.$reportGenerator = $('.report-generator');
		this.$email           = $('#email');
		this.$password        = $('#password');
		this.$jiraID        = $('#jira-id');
		this.$tempoToken      = $('#tempo-token');
		this.$extPart         = $('.ext-part');
		this.$parseStart      = $('#parseStart');
		this.$saveSettings    = $('#save-settings');
		this.$errorsContainer = $('.errors-container');
		this.$tagCloud        = $('#tag-cloud');
		this.$tagCloudItems   = $('.tag-cloud-info');
		this.$tagInput        = $('.tag-info-input');
		this.$tagAdd          = $('#tag_add');
		this.$getAccountID          = $('#get-account-id');
		this.$getAccountIDText          = $('#account-id');
		this.$avatar         = $('#avatar');

		return this;
	},

	initListeners() {
		this.$reportHash.on('blur', () => localStorage.setItem("reportHash", this.$reportHash.val()));
		this.$parseStart.on('click', () => this.sendReport());
		this.$reportDate.on('change', () => this.validation() && this.getReport(true));
		this.$jiraID.on('input', (e) => this.checkJiraID(e));
		this.$generatorTab.on('click', () => this.validation() && this.getReport(true));
		this.$tab_item.on('click', (e) => {
			let $tab = $(e.currentTarget);
			if ($tab.hasClass('block') || $tab.hasClass('active')) {
				return this;
			}
			this.$tab_item.toggleClass('active');
			this.$extPart.toggleClass('hidden');
		});

		this.$tagCloud.on('click', 'li.tag-cloud-info', (e)  => {
			this.jira_links = [];
			$(e.currentTarget).remove();
			this.$tagCloud.find('.tag-cloud-info').each((key, el)=> {
				this.jira_links.push($(el).text());
			});
			localStorage.setItem('jiraLink', JSON.stringify(this.jira_links));
			this.showJiraslinks();
		});

		this.$tagAdd.on('click', () => {
			this.jira_links = [];
			this.$tagCloud.find('.tag-cloud-info').each((key, el)=> {
				this.jira_links.push($(el).text());
			});
			localStorage.setItem('jiraLink', JSON.stringify(this.jira_links));
		});

		$('input').on('input', () => {
			this.setCredentials();
		});

		return this;
	},

	checkJiraID(){
		const jira_id = this.$jiraID.val();
		const parsed = jira_id.replace('https://', '').replace('/', '');
		this.$jiraID.val(parsed)
	},

	storeAccountID(is_error){
		const account_info = this._getAccountInfo();

		this.accountId = account_info.accountId;


		if(is_error || !account_info.displayName) {
			this.$getAccountIDText.addClass('is_error');

			this.$getAccountIDText.html('wrong jira, or you are not logged in');

		} else {
			this.$getAccountIDText.removeClass('is_error');
			this.$getAccountIDText.html(account_info.displayName);

		}

		this.$getAccountID.hide();
	},

	sendReport() {
		this.send_report.text = this.generatePreviewString(this.send_report);
		this.sayTo.bg.reportDone(this.send_report, this.report_hash);
	},

	setCredentials() {
		let credentials_obj = {
			jira_id: this.$jiraID.val(),
			tempo_token: this.$tempoToken.val(),
		},
			report_hash = this.$reportHash.val();

		localStorage.setItem('credentials', JSON.stringify(credentials_obj));
		localStorage.setItem('reportHash', report_hash);
		localStorage.setItem('jiraLink', JSON.stringify(this.jira_links));

		this.report_hash = report_hash;
		this.credentials = credentials_obj;

		this.checkAccountID();

		this.$tab_item.removeClass('block');

		return this;
	},

	checkAccountID(){
		console.log(this.credentials.jira_id);
		if(this.credentials.jira_id && this.credentials.jira_id.length) {

			this.sayTo.bg.getAccountID({
				jira_id: $.trim(this.credentials.jira_id)
			}).then((response) => {

				this._saveAccountInfo(response);
				this.storeAccountID();
				this.storeAccountImage(response.avatarUrls['32x32'])
			}).catch(error => {
				this._saveAccountInfo({});

				this.storeAccountID(true);
				this.storeAccountImage('')
			});
		}
	},

	storeAccountImage(avatar_url) {

		const image  = avatar_url ? $(`<img class="avatar-image" src="${avatar_url}"/>`) : '';
		this.$avatar.html(image);
	},

	generateDateSelect() {
		this.toDay.setDate(this.toDay.getDate() + 1);

		for (var i = 0; i < 21; i++) {

			this.toDay.setDate(this.toDay.getDate() - 1);
			let parserReportDate = this._getParserReportDate(),
				reportDate       = this._getReportDate(),
				JIRAReportDate   = this._getJiraReportData();

			this.$reportDate.append("<option data-report-date='" + parserReportDate + "' data-jira-raport-date='" + JIRAReportDate + "' value='" + reportDate + "'>" + reportDate + "</option>");
		}

		return this;
	},

	getReport(preview) {

		let reportDate = this.$reportDate.val();

		this.$trackedTime.html('reading...');

		if (!this.validateCredentials()) {
			return this;
		}

		this.$loader.show();

		this.sayTo.bg.startReportGenerator({
			jiraLinks:   this.jira_links,
			credentials: this.credentials,
			accountID: this.accountId,
			reportDate:  reportDate,
		}).done((report) => {
			this.send_report = report;
				chrome.browserAction.setBadgeText({text: String(report.total_time)});
				this.$trackedTime.html(report.total_time + 'hrs');
				this.$preview.html(this.generatePreviewString(report));
				//this.$handover.html(this.getHandoverStr(report));
				this.$errorsContainer.html();
				this.$loader.hide();
		}).fail( (errors) => {
			this.$errorsContainer.html();
			this.$errorsContainer.append('<div class="errors-title">Jira Errors:</div>')
			_.each(errors, error => this.$errorsContainer.append(`<div class="error">${error.statusText}</div>`));

			this.$loader.hide();
		});

		return this;
	},

	validation() {

		if (this.$reportHash.val().length <= 0) {
			alert('Invalid Report Hash...');
			return false;
		}
		if (this.$reportDate.val().length <= 0) {
			alert('Invalid Report Date...');
			return false;
		}

		return true;
	},

	tabsActions() {

		if (!this.validateCredentials()) {
			this.$generatorTab.addClass('block');
			this.$settings.removeClass('hidden');
		}
		else {
			this.$tab_item.toggleClass('active');
			this.$reportGenerator.removeClass('hidden');
		}

		return this;
	},

	showJiraslinks() {
		this.$tagCloud.html('');
		_.each(this.jira_links, (val) => {
			this.$tagCloud.append(`<li class="tag-cloud tag-cloud-info" >${val}</li>`)
		});

		return this;
	},

	showCredentials() {

		this.$jiraID.val(this.credentials.jira_id);
		this.$tempoToken.val(this.credentials.tempo_token);
		this.$reportHash.val(this.report_hash);

		const display_name = this._getAccountInfo().displayName;
		if(display_name) {
			this.$getAccountIDText.html(display_name);
		}

		return this;
	},

	validateCredentials() {
		return Boolean(this.credentials.tempo_token && this.credentials.tempo_token.length && this.credentials.jira_id && this.credentials.jira_id.length && this.report_hash.length);
	},

	generatePreviewString(report_obj) {
		console.log(report_obj);
		return this.getTimeStr(report_obj) + this.getCommentsStr(report_obj);
	},

	getTimeStr(report_obj) {
		let str = '';

		str += `<div><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal; font-weight: 700">Time report:</span></font></div>`;

		report_obj.issues.forEach((worklog) => {

			worklog.forEach((issue) => {
				str += `<div style="white-space: normal"><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal;">
<a href="${issue.issue_link}">${issue.issue_key}</a> - ${issue.time}</span></font></div>`;
			})
		});
		str += `<div><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal; font-weight: 700">Total: </span>${report_obj.total_time}hrs</font></div></br>`;

		return str;
	},

	getCommentsStr(report_obj){
		let str = '';

		str += `<div><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal; font-weight: 700">Today:</span></font></div>`;
		report_obj.issues.forEach((worklog) => {
			worklog.forEach((issue) => {
				str += `<div><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal;"><a href="${issue.issue_link}">${issue.issue_key}</a> - ${issue.comment}</span></font></div>`
			})
		});
		str += `<div><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal; font-weight: 700">Tomorrow: <span style="font-size: 12.8px; line-height: normal; font-weight: 500">will work on next thing for FanHub</span></font> </div>`;

		return str;
	},

	getHandoverStr(report_obj){
		let str = '';

		str += `<div><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal; font-weight: 700">Today:</span></font></div>`;
		report_obj.issues.forEach((worklog) => {
			worklog.forEach((issue) => {
				str += `<div><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal;">${issue.issue_key} - ${issue.summary}</span></font></div>`
			})
		});
		str += `<div><font color="#222222" face="arial, sans-serif"><span style="font-size: 12.8px; line-height: normal; font-weight: 700">Tomorrow: <span style="font-size: 12.8px; line-height: normal; font-weight: 500"></span></font> </div>`;

		return str;
	},

	_getParserReportDate() {
		return ("0" + (this.toDay.getDate())).slice(-2) + '/' + this.toDay.toLocaleString('en-us', {month: "short"}) + '/' + this.toDay.getFullYear().toString().substr(2, 2);
	},

	_getReportDate() {
		return this.toDay.getFullYear() + '-' + ("0" + (this.toDay.getMonth() + 1)).slice(-2) + '-' + ("0" + (this.toDay.getDate())).slice(-2);
	},

	_getJiraReportData(){
		return ("0" + (this.toDay.getMonth() + 1)).slice(-2) + '' + this.toDay.getFullYear().toString().substr(2, 2);
	},

	_saveAccountInfo(data){
		localStorage.setItem('accountInfo', JSON.stringify(data))
	},

	_getAccountInfo(){
		const data = localStorage.getItem('accountInfo') || '{}';

		return JSON.parse(data)
	},
};

/**
 * OnLoad function
 *
 * @return void
 */
window.onload = function () {
	new MainPopup();
};


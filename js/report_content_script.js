chrome.runtime.onConnect.addListener(function (port) {
    port.onMessage.addListener(factory);
});

function initialization() {
    window.popup = new popupObj();
}

function factory(obj) {
    if (obj && obj.method) {
        if (obj.data)
            window.popup[obj.method](obj.data);
        else
            window.popup[obj.method]();
    }
}

window.popupObj = function () {
};


window.popupObj.prototype = {

    /**
     * some internal params
     */
    tab_id: null,
    port: null,


    /**
     * Function will be called from bg.js
     */
    setTabId: function (id) {
        this.tab_id = id;
    },

    /**
     * Function check total host
     */
    run: function () {
        // create connection to backgroung.html and send request
        this.port = chrome.extension.connect();
        // send count of matches
        this.port.postMessage({
            method: 'reportPageReady'
        });
    },

    setReport: function (report) {
        $(document).ready(function () {
            $('#editor').html(report.text);
            $('#hours').val(report.total_time);
            $('.select2.form-control').val(report.reportDate);
        });

    },


};




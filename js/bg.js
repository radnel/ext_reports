/**
 * OnLoad function
 *
 * @return void
 */
window.onload = function () {

	// tmp storage
	window.bg = new bgObj();

	// set handler to tabs
	chrome.tabs.onActivated.addListener(function (info) {
		//alert('Перешел на новую вкладку');
		window.bg.initTabConnection(info);
		window.bg.active_tab = info;

	});

	// set handler to tabs:  need for seng objects
	chrome.runtime.onConnect.addListener(function (port) {
		//alert('Прием данных, посланных от jira_content_script.js с помощью port');
		port.onMessage.addListener(factory);
	});

	// set handler to extention on icon click
	chrome.browserAction.onClicked.addListener(function (tab) {
		//alert('Клик по иконке Расширения(работает только если default_popup не задан)');
		window.bg.onClicked(tab);
		console.log(window.bg)
	});

	// set handler to tabs
	chrome.tabs.onUpdated.addListener(function (id, info, tab) {
		//alert('Полная загрузка страницы onUpdated');
		// if tab load
		if (info && info.status && (info.status.toLowerCase() === 'complete')) {
			// if user open empty tab or ftp protocol and etc.
			if (!id || !tab || !tab.url || ((tab.url.indexOf('http:') == -1) && (tab.url.indexOf('https:') == -1)))
				return 0;
			// save tab info if need
			window.bg.initTabConnection(tab);

			//// connect with new tab, and save object
			//var port = chrome.tabs.connect(id);
			//window.bg.tabs[id].port_info = port;

			var content_scripts = chrome.runtime.getManifest().content_scripts;
			$.each(content_scripts, function (key, scripts) {
				$.each(scripts.matches, function (key, match) {
					match = match.replace(/\./g, '\\.').replace(/\*/g, '.*').replace(/\//g, '\\/');
					var re = new RegExp(match, 'g');
					if (re.test(tab.url)) {
						// run function in popup.html
						chrome.tabs.executeScript(id, { code: "initialization()" });
						// send id, hosts and others information into popup.js
						window.bg.tabs[id].port_info.postMessage({ method: 'setTabId', data: id });
						window.bg.tabs[id].port_info.postMessage({ method: 'run' });
					}
				});
			});

			chrome.browserAction.setPopup({ popup: "main-popup.html" });
		}

	});

	chrome.runtime.onSuspend && chrome.runtime.onSuspend.addListener(function () {
		localStorage.setItem('credentials', '');
	});

	window.bg.onAppReady();
};

/**
 * Functino will be called when popup.js send some data by port interface
 *
 * @return void
 */
function factory(obj) {
	if (obj && obj.method) {
		if (obj.data)
			window.bg[obj.method](obj.data);
		else
			window.bg[obj.method]();
	}
}

/**
 * Popup object
 *
 * @version 2013-10-11
 * @return  Object
 */
window.bgObj = function () {
};

/**
 * Pablic methods
 */
window.bgObj.prototype = {

	/**
	 * some internal params
	 */
	tabs: {},
	active_tab: {},
	report: {},
	reportConf: {
		reportDate: null,
		reportHash: null,
		JIRAReportDate: null,
		parserReportDate: null
	},
	reports_responses: [],
	errors: [],
	startReportGenerator: function (config, isPreview) {

		this.reports_responses = [];
		this.errors            = [];
		this.reportConf        = config;
		this.parser = new DOMParser();
		var main               = $.Deferred(),
			defers             = [],
			report             = {
				total_time: 0,
				issues:     []
			};


		var JIRA_url = 'https://api.tempo.io/core/3/worklogs/user/' + this.reportConf.accountID,
				defer    = this.getReportHTML(JIRA_url, this.reportConf);
			defers.push(defer);


		$.when.apply(null, defers).then(() => {
				_.each(this.reports_responses, (val, key) => {
					if(val.results){
						report.total_time += this.getTotalTime(val.results);
						report.issues.push(this.getIssueObj(val.results));
					}

				});

				report.total_time  = parseFloat(report.total_time / 3600);

			console.log(report);
			main.resolve(report);

			}
		).fail(() => {
			main.reject(this.errors);
		});

		return main.promise();
	},

	getTotalTime(issues) {
		let total_seconds = 0;
		_.each(issues, (val) => {
			total_seconds += val.timeSpentSeconds;
		});

		return total_seconds;
	},

	getIssueObj(issues) {
		let issues_obj = [];

		console.log();
		if (issues) {
			_.each(issues, (val) => {
				issues_obj.push({
					issue_key: val.issue.key,
					issue_link: 'https://' + this.reportConf.credentials.jira_id + '/browse/' + val.issue.key,
					time: parseFloat(val.timeSpentSeconds / 3600).toFixed(2) + 'hrs',
					comment: val.description,
					summary: val.description
				});
			});
		}


		return issues_obj;
	},

	getAccountID: function (options) {
		return this.fetchAccountID(options);
	},

	fetchAccountID(options) {
		return new Promise((r, rj) => {
			$.ajax({
				type: 'GET',
				url: 'https://' + options.jira_id + '/rest/api/3/myself'
			})
			.success(response => r(response))
			.fail(error => {
				rj(error)
			})
		})
	},

	getReportHTML: function (jiraUrl, config, jira_base_link) {
		let data = this.getRequestData(jira_base_link, config);

		return $.ajax({
				type: 'GET',
				url: jiraUrl,
				data: data,
				beforeSend(xhr) {
					xhr.setRequestHeader("Authorization", "Bearer " + config.credentials.tempo_token);
				}
			})
			.success(response => this.reports_responses.push(response))
			.fail(error => {
				this.errors.push(error)
			});
	},

	getRequestData(jira_base_link, config) {

		let data = {
			from: config.reportDate,
			to: config.reportDate,
			updatedFrom: config.reportDate,
		};

		if (config.credentials.user_name) {
			data.accountId = config.credentials.user_name;
		}

		return data;
	},

	reportDone: function (data, hash) {
		this.report = data;
		var newURL = "http://ben.zerp.info/jobReportsRef/" + hash;
		chrome.tabs.create({ url: newURL });
	},

	reportPageReady: function () {
		var port = chrome.tabs.connect(this.active_tab.tabId);
		this.report.reportDate = this.reportConf.reportDate;
		port.postMessage({ method: 'setReport', data: this.report });
	},
	/**
	 * init() function
	 */
	onAppReady: function () {
		// if user not logged into application set login.html popup
		chrome.browserAction.setPopup({ popup: "main-popup.html" });
	},

	/**
	 * Function will be called from login.js and others places
	 */
	setPopup: function (popup_file) {
		chrome.browserAction.setPopup({ tabId: this.active_tab.tabId, popup: popup_file });
	},

	/**
	 * Function add tab into $tabs object, if need
	 */
	initTabConnection: function (tab) {
		var tabID = (tab.id && (tab.id != 0)) ? tab.id : (tab.tabId && (tab.tabId != 0)) ? tab.tabId : null;
		if (tabID) {
			if (!this.tabs[tabID])
				this.tabs[tabID] = { tab_obj: tab };
		}

		var port = chrome.tabs.connect(tabID);
		this.tabs[tabID].port_info = port;
	},

	/**
	 * Function will be called when user click on extension icon
	 */
	onClicked: function (tab) {
		alert('Произошла ошибка. Обратитесь к разработчикам данного приложения.');
		return 0;
	}
};
